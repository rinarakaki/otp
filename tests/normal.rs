use otp::algorithm;

const TIMES: [u64; 6] = [
    59,
    1111111109,
    1111111111,
    1234567890,
    2000000000,
    20000000000,
];

#[test]
fn time_based_counter_bytes_works() {
    const COUNTS: [u64; 6] = [
        1, 0x023523ec, 0x023523ed, 0x0273ef07, 0x03f940aa, 0x27bc86aa,
    ];
    for (&time, count) in TIMES.iter().zip(COUNTS.iter()) {
        assert_eq!(
            algorithm::time_based_counter_bytes(time, 0, 30),
            count.to_be_bytes()
        );
    }
}

#[test]
fn time_based_counter_bytes_extended_tests() {
    use algorithm::time_based_counter_bytes;
    assert_eq!(time_based_counter_bytes(0, 0, 10), 0u64.to_be_bytes());
    assert_eq!(time_based_counter_bytes(1, 0, 10), 0u64.to_be_bytes());
    assert_eq!(time_based_counter_bytes(60, 0, 30), 2u64.to_be_bytes());
    assert_eq!(time_based_counter_bytes(61, 0, 30), 2u64.to_be_bytes());
}

#[test]
#[should_panic]
fn time_based_counter_bytes_panic() {
    assert_eq!(
        algorithm::time_based_counter_bytes(59, 100, 30),
        2u64.to_be_bytes()
    );
}

#[test]
fn rfc6238_vectors_works() {
    const RESULTS: [(&str, &str, &str); 6] = [
        ("94287082", "46119246", "90693936"),
        ("07081804", "68084774", "25091201"),
        ("14050471", "67062674", "99943326"),
        ("89005924", "91819424", "93441116"),
        ("69279037", "90698825", "38618901"),
        ("65353130", "77737706", "47863826"),
    ];
    let key_sha1: &[u8; 20] = b"12345678901234567890";
    let key_sha256 = [&key_sha1[..], &key_sha1[..12]].concat();
    let key_sha512 = [&key_sha1[..], &key_sha1[..], &key_sha1[..], &key_sha1[..4]].concat();
    for (&time, result) in TIMES.iter().zip(RESULTS.iter()) {
        assert_eq!(generate_totp(time, &key_sha1[..], "SHA-1"), result.0);
        assert_eq!(generate_totp(time, &key_sha256[..], "SHA256"), result.1);
        assert_eq!(generate_totp(time, &key_sha512[..], "sha512"), result.2);
    }
}

fn generate_totp(time: u64, key: &[u8], h: &str) -> String {
    use algorithm::{hmac_sha, time_based_counter_bytes, truncate};
    let count = time_based_counter_bytes(time, 0, 30);
    let mac: Vec<_> = hmac_sha(&count, key, h.into());
    truncate(&mac[..], 8)
}
