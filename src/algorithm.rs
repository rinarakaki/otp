//! Low-level APIs for TOTP code generation.
//!
//! Don't use these APIs directly unless you know exactly what you are doing.

use core::{
    convert::TryInto,
    iter::FromIterator
};

use clap::ValueEnum;
use ring::hmac;

/**
Hash functions for HMAC supported in
[RFC 6238 Section 1.2](https://www.rfc-editor.org/rfc/rfc6238#section-1.2).

So far, only SHA-1, SHA-256 and SHA-512 are supported.

# Example
```
use otp::Algorithm;

let a = Algorithm::Sha256;
// This `Algorithm` can be converted from `str` case-insensitively.
let b = Algorithm::from("SHA-256");
let c: Algorithm = "sha256".into();

assert_eq!(a, b);
assert_eq!(a, c);
```
*/
#[derive(PartialEq, Eq, Clone, Copy, Debug, ValueEnum)]
pub enum Algorithm {
    Sha1,
    Sha256,
    Sha512,
}

impl<T: AsRef<str>> From<T> for Algorithm {
    fn from(x: T) -> Self {
        let t: &str = x.as_ref();
        match t.to_ascii_lowercase().as_ref() {
            "sha1" | "sha-1" => Algorithm::Sha1,
            "sha256" | "sha-256" => Algorithm::Sha256,
            "sha512" | "sha-512" => Algorithm::Sha512,
            _ => panic!("{} is not a acceptable hash algorithm", t),
        }
    }
}

impl Algorithm {
    pub fn as_str(&self) -> &'static str {
        match self {
            Algorithm::Sha1 => "sha1",
            Algorithm::Sha256 => "sha256",
            Algorithm::Sha512 => "sha512",
        }
    }
}

impl Into<hmac::Algorithm> for Algorithm {
    fn into(self) -> hmac::Algorithm {
        match self {
            Algorithm::Sha1 => hmac::HMAC_SHA1_FOR_LEGACY_USE_ONLY,
            Algorithm::Sha256 => hmac::HMAC_SHA256,
            Algorithm::Sha512 => hmac::HMAC_SHA512,
        }
    }
}

/**
The counter as a time factor for TOTP defined in [RFC 6238 Section 4].

Arguments:  
In this function, 64-bit unsigned integer (u64) is used to store the Unix
timestamp.
- `current`: Unix timestamp of the current time.
- `t0`: Unix timestamp of the initial counter time T0 (default value in
[RFC 6238 Section 4] is `0`).
- `step`: The time step in seconds (default value in [RFC 6238 Section 4]
is `30`).

Return:
- an array of 8 bytes contains the counter value, which represents the
number of time steps between the initial counter time T0 and the current
Unix time.

# Panics

Panics if `now` is less than `t0` or `step` is zero.

# Example

```
use otp::algorithm::time_based_counter_bytes;

// 59 is the Unix timestamp of "1970-01-01 00:00:59 UTC"
let output = time_based_counter_bytes(59, 0, 30);

assert_eq!(output, 1u64.to_be_bytes());
```

[RFC 6238 Section 4]: https://www.rfc-editor.org/rfc/rfc6238#section-4
*/
#[inline(always)]
pub fn time_based_counter_bytes(now: u64, t0: u64, step: u64) -> [u8; 8] {
    time_based_counter_number(now, t0, step).to_be_bytes()
}

#[inline(always)]
pub(crate) fn time_based_counter_number(now: u64, t0: u64, step: u64) -> u64 {
    assert!(t0 <= now);
    (now - t0) / step
}

/**
Compute the HMAC bytes for given bytes.

Arguments:
- `message`: bytes of the message.
- `key`: bytes of the key.
- `algorithm`: specify the hash function using the [`Algorithm`] enum.

Return:
- a [collection](https://doc.rust-lang.org/stable/std/iter/trait.Iterator.html#method.collect)
of HMAC bytes transformed from an iterator.

# Example

```
use otp::{algorithm::hmac_sha, Algorithm};

// Source of the vectors: https://www.rfc-editor.org/rfc/rfc4231.html#section-4.2
let expected = &[
    0xb0, 0x34, 0x4c, 0x61, 0xd8, 0xdb, 0x38, 0x53, 0x5c, 0xa8,
    0xaf, 0xce, 0xaf, 0x0b, 0xf1, 0x2b, 0x88, 0x1d, 0xc2, 0x00,
    0xc9, 0x83, 0x3d, 0xa7, 0x26, 0xe9, 0x37, 0x6c, 0x2e, 0x32,
    0xcf, 0xf7_u8,
];

let m = b"Hi There";
let k = &[0x0b_u8; 20];
let output: Vec<_> = hmac_sha(m, k, Algorithm::Sha256);

assert_eq!(&output[..], expected)
```

# Note

It would be better that the size of the key is the same as the output
length of the hash function, based on the recommendation in
[RFC 2104 Section 3](https://www.rfc-editor.org/rfc/rfc2104#section-3).  
However, shorter or longer keys are allowed. For more information on
handling such keys, please refer to the
[document of `ring::hmac::Key::new`](https://docs.rs/ring/latest/ring/hmac/struct.Key.html#method.new).
*/
pub fn hmac_sha<T>(message: &[u8], key: &[u8], algorithm: Algorithm) -> T
    where T: FromIterator<u8>
{
    let key = hmac::Key::new(algorithm.into(), key);
    let mut context = hmac::Context::with_key(&key);
    context.update(message.as_ref());
    let tag = context.sign();
    tag.as_ref().iter().cloned().collect()
}

/**
The `Truncate` function defined in [RFC 4226 Section 5.3](https://www.rfc-editor.org/rfc/rfc4226#section-5.3):
extract 31 bits from the HMAC bytes and truncate the lowest decimal digits to a String.

Arguments:
- `bytes`: bytes of the HMAC output.
- `digits`: the length of TOTP code.

Return:
- TOTP code in String.

# Panics

Panics if `digits` is zero.

# Example

```
use otp::algorithm::truncate;
// these test vectors come form https://www.rfc-editor.org/rfc/rfc4226#section-5.4
let expected = "872921";

let h = &[
    0x1f, 0x86, 0x98, 0x69, 0x0e, 0x02, 0xca, 0x16, 0x61, 0x85,
    0x50, 0xef, 0x7f, 0x19, 0xda, 0x8e, 0x94, 0x5b, 0x55, 0x5a_u8,
];
let d = 6usize;
let output = truncate(h, d);

assert_eq!(&output[..], expected);
```
*/
#[inline]
pub fn truncate(bytes: &[u8], digits: usize) -> String {
    assert!(0 < digits);
    let offset = (0x0f & bytes.last().expect("the `bytes` is empty")) as usize;
    let bin_code =
        u32::from_be_bytes(bytes[offset..=offset + 3].try_into().unwrap()) as usize;
    let a = (bin_code & 0x7fff_ffff) % 10usize.pow(digits as u32);
    format!("{:0>1$}", a, digits)
}
