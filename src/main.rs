use clap::Parser;

use otp::Totp;

#[derive(Parser)]
struct Args {
    secret: String,
    #[command(flatten)]
    totp: Totp
}

fn main() {
    let args = Args::parse();
    let totp = args.totp;
    
    let output1 = totp.code(&args.secret.as_bytes().to_vec());
    println!("TOTP code for current time: {}", output1);
    
    let output2 = totp.next_update_time().unwrap();
    println!("Next update will be at the unix timestamp of {}", output2);
    
    let output3 = totp.code_window(&args.secret.as_bytes().to_vec(), -5..=5).unwrap();
    println!("Codes for 5 minutes earlier or later are:");
    for i in output3 {
        println!("  {}", i);
    }
}
