//! High-level APIs for TOTP code generation.

use std::time::{SystemTime, UNIX_EPOCH};

use clap::Args;

use crate::algorithm::{
    hmac_sha, time_based_counter_bytes, time_based_counter_number, truncate, Algorithm,
};

/**
TOTP code generator

# Why this struct doesn't store the keys?

- The `key` is the **secret credential** of TOTP.
- For some reasons, programmers may consider keeping [Totp] instances in memory for a period of time.
- However, the keys should not be kept in memory for a long time if they do
not need to be used during this time. Especially for those devices with a
certain secure storage area, storing the keys in the memory for a long time
weakens the security system.
- Therefore, we recommend: the `key` is only loaded into memory when needed.
And, when the operation is done, use some reliable method to overwrite the
memory area corresponding to `key`. (For example, the crate
[zeroize](https://crates.io/crates/zeroize) might be helpful for this)
- The details of security can be very complicated, and we can't include all
of them here. If you are interested, you can check the relevant information
yourself.
*/
#[derive(PartialEq, Eq, Clone, Copy, Debug, Args)]
pub struct Totp {
    #[arg(skip)]
    current: Option<u64>,
    /// Length of the TOTP code.
    #[arg(short, long, default_value_t = 6)]
    digits: usize,
    // TODO(rnarkk) `duration` or `period` or `interval`
    /// Update time interval in seconds (X).
    #[arg(short, long, default_value_t = 30)]
    step: u64,
    /// Unix time to start counting time steps (T0).
    #[arg(short, long, default_value_t = 0)]
    t0: u64,
    #[arg(short, long, value_enum, default_value_t = Algorithm::Sha1)]
    algorithm: Algorithm,
}

impl Default for Totp {
    /// Default values defined in
    /// [RFC 6238 Section 4](https://www.rfc-editor.org/rfc/rfc6238#section-4).
    fn default() -> Self {
        Totp {
            current: None,
            digits: 6,
            step: 30,
            t0: 0,
            algorithm: Algorithm::Sha1,
        }
    }
}

impl Totp {
    pub fn new(digits: usize, step: u64, t0: u64, algorithm: Algorithm) -> Self {
        assert!(0 < step);
        assert!(0 < digits && digits <= 10);
        Totp {
            current: None,
            digits,
            step,
            t0,
            algorithm,
        }
    }

    /// Set a new value to the field `step`. 
    pub fn with_step(mut self, step: u64) -> Self {
        assert!(0 < step);
        self.step = step;
        self
    }

    /**
    `digits` must be between `1` and `10`.
    This is because a 31-bit unsigned integer has a maximum of 10 decimal
    digits. In [RFC 4226 Section 5.3](https://www.rfc-editor.org/rfc/rfc4226#section-5.3),
    the recommended values are `6` ~ `8`. But here we give you more choices.
    */
    pub fn with_digit(mut self, digits: usize) -> Self {
        // max of 31-bit unsigned integer is 10-digit in decimal
        assert!(0 < digits && digits <= 10);
        self.digits = digits;
        self
    }

    /// Set a new value to the field `t0`. 
    pub fn with_t0(mut self, t0: u64) -> Self {
        self.t0 = t0;
        self
    }

    /// Set a new value to the field `algorithm`.
    pub fn with_algorithm(mut self, algorithm: Algorithm) -> Self {
        self.algorithm = algorithm;
        self
    }

    /// Generate the TOTP code using the given key bytes.
    ///
    /// # Panics
    /// 
    /// Panics if the current system time is earlier than the Unix epoch
    /// (1970-01-01T00:00:00Z) and this instance of Totp is using the
    /// system time.
    pub fn code(&self, key: &[u8]) -> String {
        self.get_code_with(key, || Self::get_target_time(self.current))
    }

    /// Generate the TOTP code using a closure to specify the time (For example,
    /// getting network time instead of using system time).
    pub fn get_code_with<F: Fn() -> u64>(&self, key: &[u8], f: F) -> String {
        let current = f();
        let count = time_based_counter_bytes(current, self.t0, self.step);
        let mac: Vec<_> = hmac_sha(&count, key, self.algorithm);
        truncate(&mac[..], self.digits)
    }

    /**
    Generate a window of contiguous TOTP codes (This may be helpful for
    time tolerance).  
    This returns `None` if the iterator `window` cannot produce a valid
    TOTP counter value.

    # Example

    ```
    use std::thread::sleep;
    use std::time::Duration;

    use otp::Totp;

    let shared_key = b"12345678901234567890";
    // a fast TOTP that updates every seconds
    let client_totp = Totp::default().with_step(1);
    let client_code = client_totp.code(shared_key);

    // Let's simulate the time difference.
    sleep(Duration::from_millis(800));

    let server_totp = Totp::default().with_step(1);
    // This provides time tolerance of -2 ~ +2 period.
    let server_code = server_totp.code_window(shared_key, -2..=2).unwrap();
    assert!(server_code.iter().any(|x| x == client_code.as_str()));
    ```

    # Panics

    Panics if the current system time is earlier than the Unix epoch
    (1970-01-01T00:00:00Z) and this instance of Totp is using the
    system time.
    */
    pub fn code_window<T>(&self, key: &[u8], window: T)
        -> Option<Vec<String>>
        where T: Iterator<Item = isize>
    {
        self.code_window_with(key, window, || Self::get_target_time(self.current))
    }
    
    /// Generate a window of contiguous TOTP codes using a closure to specify
    /// the time.
    pub fn code_window_with<T, F>(&self, key: &[u8], window: T, f: F)
        -> Option<Vec<String>>
        where T: Iterator<Item = isize>, F: Fn() -> u64
    {
        let current = f();
        let origin_count = time_based_counter_number(current, self.t0, self.step);
        let mut output = Vec::new();
        for i in window {
            let tmp_count = if i < 0 {
                match origin_count.checked_sub((-i) as u64) {
                    Some(x) => x,
                    None => continue,
                }
            } else {
                match origin_count.checked_add(i as u64) {
                    Some(x) => x,
                    None => continue,
                }
            }
            .to_be_bytes();
            let mac: Vec<_> = hmac_sha(&tmp_count, key, self.algorithm);
            output.push(truncate(&mac[..], self.digits));
        }
        if output.is_empty() {
            None
        } else {
            Some(output)
        }
    }
    
    /**
    Get the next timestamp when the TOTP code will be updated.  
    This returns `None` if timestamp goes over the maximum of 64-bit
    unsigned integer.

    # Panics

    Panics if the current system time is earlier than the Unix epoch
    (1970-01-01T00:00:00Z) and this instance of Totp is using the
    system time.
    */
    pub fn next_update_time(&self) -> Option<u64> {
        let this_time = Self::get_target_time(self.current);
        let this_count = time_based_counter_number(this_time, self.t0, self.step);
        this_count
            .checked_add(1)
            .and_then(|x| x.checked_mul(self.step))
            .and_then(|x| x.checked_add(self.t0))
    }
    
    /**
    Get the target time of this instance of Totp.

    # Panics

    Panics if the current system time is earlier than the Unix epoch
    (1970-01-01T00:00:00Z) and this instance of Totp is using the
    system time.
    */
    #[inline(always)]
    fn get_target_time(time: Option<u64>) -> u64 {
        match time {
            Some(t) => t,
            None => SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .expect("Time went backwards")
                    .as_secs()
        }
    }
}
