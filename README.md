# otp

One-time password ([RFC 6238](https://www.rfc-editor.org/rfc/rfc6238)) CLI.
Forked from https://github.com/KaneGreen/totp_rfc6238.

# TODO

- Prompt to input <SECRET> instead of taking it as a visible command argument

# Install

```sh
cargo install --git https://github.com/synthet-ic/otp
```

# Usage

```sh
Usage: otp [OPTIONS] <SECRET>

Arguments:
  <SECRET>
          

Options:
  -d, --digits <DIGITS>
          Length of the TOTP code
          
          [default: 6]

  -s, --step <STEP>
          Update time interval in seconds
          
          [default: 30]

  -t, --t0 <T0>
          Unix timestamp of the initial counter time T0
          
          [default: 0]

  -a, --algorithm <ALGORITHM>
          [default: sha1]
          [possible values: sha1, sha256, sha512]

  -h, --help
          Print help (see a summary with '-h')
```
